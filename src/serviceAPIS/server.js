import axios from 'axios'

export default () => {
  var api = axios.create({
    baseURL: 'http://localhost:3000/'
  })
  return api
}
