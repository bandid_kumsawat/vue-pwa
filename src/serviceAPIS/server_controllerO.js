import axios from 'axios'

export default () => {
  var api = axios.create({
    // baseURL: 'http://35.242.147.241:8081/'
    baseURL: 'http://localhost:3000/api/'
    // baseURL: 'http://209.97.163.214:4001/'
    // baseURL: 'http://35.247.242.59:3000/api/'
    // baseURL: 'http://localhost:1880/'
  })
  return api
}
